/*Declaracion de datos a transferir entre el cliente y el servidor*/
/*Declaracion de constantes*/
const MAXNOM = 30;
const MAXINDENTIFICACION = 10;
const MAXUSUARIO = 10;
const MAXCONTRASENIA = 10;
const MAXDATO= 20;
const MAXMOD= 12;
const MAXFECHA= 10;
const TADMIN =0;
const TESTUDIANTE =1;
const TDIRECTOR =2;
const TEVALUDADOR =3;
typedef struct nodo_usuario *proxNodoUsuario;
/*Declaracion de la estructura que permite almacenar los datos de un usuario*/
struct nodo_usuario{
char nombres[MAXNOM];
int identificacion;
char usuario_unicauca[MAXUSUARIO];
char contrasenia[MAXCONTRASENIA];
int tipo;
proxNodoUsuario nodoSiguiente;/*Atributo del nodo usuario que le permite guardar la referencia al
siguiente nodo*/
};
/*Definicion de las operaciones que se pueden realizar*/
program gestion_usuarios{
version gestion_usuarios_version{
bool registrarUsuario(nodo_usuario objUsuario)=1;
int log_usuario(nodo_usuario objUsuario) =3;
bool cambiar_contrasenia_admin(nodo_usuario objAdmin) =4;
}=3;
}=0x20000002;
