/*Declaracion de datos a transferir entre el cliente y el servidor*/
/*Declaracion de constantes*/
const MAXNOM = 30;
const MAXTIT = 40;
const MAXDATO= 20;
const MAXMOD= 12;
const MAXFECHA= 10;
typedef struct nodo_anteproyecto *proxNodoAnteproyecto;
typedef struct nodo_evaluacion_ant *proxNodoEvaluacionAnt;
/*Declaracion de la estructura que permite almacenar los datos de las evaluanciones de los anteproyectos*/
struct nodo_evaluacion_ant{
char nombre_evaluador1[MAXNOM];
char concepto_evaluador1[MAXDATO];
char fecha_revicion1[MAXFECHA];
char nombre_evaluador2[MAXNOM];
char concepto_evaluador2[MAXDATO];
char fecha_revicion2[MAXFECHA];
int codigo_anteproyecto;
proxNodoEvaluacionAnt nodoSiguiente;/*Atributo del nodo evaluador que le permite guardar la referencia al
siguiente nodo*/
};
/*Declaracion de la estructura que permite almacenar los datos de un anteproyecto*/
struct nodo_anteproyecto{
int num_revicion;
int estado;
int concepto;
char fecha_aprobacion[MAXFECHA];
char fecha_registro[MAXFECHA];
char nombre_codir[MAXNOM];
char nombre_dir[MAXNOM];
char nombre_estud2[MAXNOM];
char nombre_estud1[MAXNOM];
int codigo;
char titulo[MAXTIT];
char modalidad[MAXMOD];
nodo_evaluacion_ant concepto_evaluadores;
proxNodoAnteproyecto nodoSiguiente;/*Atributo del nodo anteproyecto que le permite guardar la referencia al
siguiente nodo*/
};


/*Definicion de las operaciones que se pueden realizar*/
program gestion_anteproyectos{
version gestion_anteproyectos_version{
nodo_anteproyecto consultarAnteproyecto(int codigo)=1;
bool registrarAnteproyecto(nodo_anteproyecto objAnteproyecto)=2;
bool registrarEvaludoresAnt(nodo_evaluacion_ant objEvaluaciones)=3;
proxNodoAnteproyecto listarAnteproyectos(void)=4;
bool addConceptoAnteproyectoJefe(nodo_anteproyecto oAjSnteproyecto)=5;
bool addConceptoAnteproyecto(nodo_evaluacion_ant objEvaluaciones) =6;
proxNodoAnteproyecto listarAnteproyectosAprobados(void)=7;
proxNodoAnteproyecto listarAnteproyectosNoAprobados(void)=8;
}=2;
}=0x20000001;
